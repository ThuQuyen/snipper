using UnityEngine;
using UnityEngine.EventSystems;

public class TouchInputManager : MonoBehaviour
{
    public Transform Scope;
    public GameObject text;
    public GameObject shootButton;

    public EventTrigger trigger;

    private void Start()
    {
        EventTrigger.Entry clickEntry = new EventTrigger.Entry();
        clickEntry.eventID = EventTriggerType.PointerClick;
        clickEntry.callback.AddListener((data) => { OnPointerClick((PointerEventData)data); });
        trigger.triggers.Add(clickEntry);

        EventTrigger.Entry dragEntry = new EventTrigger.Entry();
        dragEntry.eventID = EventTriggerType.Drag;
        dragEntry.callback.AddListener((data) => { OnPointerClick((PointerEventData)data); });
        trigger.triggers.Add(dragEntry);
    }

    public void OnPointerClick(PointerEventData data)
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            if (EventSystem.current.currentSelectedGameObject == shootButton)
                return;
        }

        Scope.gameObject.GetComponent<Animator>().enabled = false;
        Scope.rotation = Quaternion.identity;

        var screenPos = new Vector3(data.position.x, data.position.y, Camera.main.nearClipPlane);
        //var worldPos = Camera.main.ScreenToWorldPoint(screenPos);
        Scope.position = screenPos;

        text.SetActive(false);
    }
}
