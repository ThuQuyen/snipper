var Deserializers = {}
Deserializers["UnityEngine.JointSpring"] = function (request, data, root) {
  var i3330 = root || request.c( 'UnityEngine.JointSpring' )
  var i3331 = data
  i3330.spring = i3331[0]
  i3330.damper = i3331[1]
  i3330.targetPosition = i3331[2]
  return i3330
}

Deserializers["UnityEngine.JointMotor"] = function (request, data, root) {
  var i3332 = root || request.c( 'UnityEngine.JointMotor' )
  var i3333 = data
  i3332.m_TargetVelocity = i3333[0]
  i3332.m_Force = i3333[1]
  i3332.m_FreeSpin = i3333[2]
  return i3332
}

Deserializers["UnityEngine.JointLimits"] = function (request, data, root) {
  var i3334 = root || request.c( 'UnityEngine.JointLimits' )
  var i3335 = data
  i3334.m_Min = i3335[0]
  i3334.m_Max = i3335[1]
  i3334.m_Bounciness = i3335[2]
  i3334.m_BounceMinVelocity = i3335[3]
  i3334.m_ContactDistance = i3335[4]
  i3334.minBounce = i3335[5]
  i3334.maxBounce = i3335[6]
  return i3334
}

Deserializers["UnityEngine.JointDrive"] = function (request, data, root) {
  var i3336 = root || request.c( 'UnityEngine.JointDrive' )
  var i3337 = data
  i3336.m_PositionSpring = i3337[0]
  i3336.m_PositionDamper = i3337[1]
  i3336.m_MaximumForce = i3337[2]
  return i3336
}

Deserializers["UnityEngine.SoftJointLimitSpring"] = function (request, data, root) {
  var i3338 = root || request.c( 'UnityEngine.SoftJointLimitSpring' )
  var i3339 = data
  i3338.m_Spring = i3339[0]
  i3338.m_Damper = i3339[1]
  return i3338
}

Deserializers["UnityEngine.SoftJointLimit"] = function (request, data, root) {
  var i3340 = root || request.c( 'UnityEngine.SoftJointLimit' )
  var i3341 = data
  i3340.m_Limit = i3341[0]
  i3340.m_Bounciness = i3341[1]
  i3340.m_ContactDistance = i3341[2]
  return i3340
}

Deserializers["UnityEngine.WheelFrictionCurve"] = function (request, data, root) {
  var i3342 = root || request.c( 'UnityEngine.WheelFrictionCurve' )
  var i3343 = data
  i3342.m_ExtremumSlip = i3343[0]
  i3342.m_ExtremumValue = i3343[1]
  i3342.m_AsymptoteSlip = i3343[2]
  i3342.m_AsymptoteValue = i3343[3]
  i3342.m_Stiffness = i3343[4]
  return i3342
}

Deserializers["UnityEngine.JointAngleLimits2D"] = function (request, data, root) {
  var i3344 = root || request.c( 'UnityEngine.JointAngleLimits2D' )
  var i3345 = data
  i3344.m_LowerAngle = i3345[0]
  i3344.m_UpperAngle = i3345[1]
  return i3344
}

Deserializers["UnityEngine.JointMotor2D"] = function (request, data, root) {
  var i3346 = root || request.c( 'UnityEngine.JointMotor2D' )
  var i3347 = data
  i3346.m_MotorSpeed = i3347[0]
  i3346.m_MaximumMotorTorque = i3347[1]
  return i3346
}

Deserializers["UnityEngine.JointSuspension2D"] = function (request, data, root) {
  var i3348 = root || request.c( 'UnityEngine.JointSuspension2D' )
  var i3349 = data
  i3348.m_DampingRatio = i3349[0]
  i3348.m_Frequency = i3349[1]
  i3348.m_Angle = i3349[2]
  return i3348
}

Deserializers["UnityEngine.JointTranslationLimits2D"] = function (request, data, root) {
  var i3350 = root || request.c( 'UnityEngine.JointTranslationLimits2D' )
  var i3351 = data
  i3350.m_LowerTranslation = i3351[0]
  i3350.m_UpperTranslation = i3351[1]
  return i3350
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Material"] = function (request, data, root) {
  var i3352 = root || new pc.UnityMaterial()
  var i3353 = data
  i3352.name = i3353[0]
  request.r(i3353[1], i3353[2], 0, i3352, 'shader')
  i3352.renderQueue = i3353[3]
  i3352.enableInstancing = !!i3353[4]
  var i3355 = i3353[5]
  var i3354 = []
  for(var i = 0; i < i3355.length; i += 1) {
    i3354.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Material+FloatParameter', i3355[i + 0]) );
  }
  i3352.floatParameters = i3354
  var i3357 = i3353[6]
  var i3356 = []
  for(var i = 0; i < i3357.length; i += 1) {
    i3356.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Material+ColorParameter', i3357[i + 0]) );
  }
  i3352.colorParameters = i3356
  var i3359 = i3353[7]
  var i3358 = []
  for(var i = 0; i < i3359.length; i += 1) {
    i3358.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Material+VectorParameter', i3359[i + 0]) );
  }
  i3352.vectorParameters = i3358
  var i3361 = i3353[8]
  var i3360 = []
  for(var i = 0; i < i3361.length; i += 1) {
    i3360.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Material+TextureParameter', i3361[i + 0]) );
  }
  i3352.textureParameters = i3360
  var i3363 = i3353[9]
  var i3362 = []
  for(var i = 0; i < i3363.length; i += 1) {
    i3362.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Material+MaterialFlag', i3363[i + 0]) );
  }
  i3352.materialFlags = i3362
  return i3352
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Material+FloatParameter"] = function (request, data, root) {
  var i3366 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Material+FloatParameter' )
  var i3367 = data
  i3366.name = i3367[0]
  i3366.value = i3367[1]
  return i3366
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Material+ColorParameter"] = function (request, data, root) {
  var i3370 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Material+ColorParameter' )
  var i3371 = data
  i3370.name = i3371[0]
  i3370.value = new pc.Color(i3371[1], i3371[2], i3371[3], i3371[4])
  return i3370
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Material+VectorParameter"] = function (request, data, root) {
  var i3374 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Material+VectorParameter' )
  var i3375 = data
  i3374.name = i3375[0]
  i3374.value = new pc.Vec4( i3375[1], i3375[2], i3375[3], i3375[4] )
  return i3374
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Material+TextureParameter"] = function (request, data, root) {
  var i3378 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Material+TextureParameter' )
  var i3379 = data
  i3378.name = i3379[0]
  request.r(i3379[1], i3379[2], 0, i3378, 'value')
  return i3378
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Material+MaterialFlag"] = function (request, data, root) {
  var i3382 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Material+MaterialFlag' )
  var i3383 = data
  i3382.name = i3383[0]
  i3382.enabled = !!i3383[1]
  return i3382
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.Transform"] = function (request, data, root) {
  var i3384 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.Transform' )
  var i3385 = data
  i3384.position = new pc.Vec3( i3385[0], i3385[1], i3385[2] )
  i3384.scale = new pc.Vec3( i3385[3], i3385[4], i3385[5] )
  i3384.rotation = new pc.Quat(i3385[6], i3385[7], i3385[8], i3385[9])
  return i3384
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.Camera"] = function (request, data, root) {
  var i3386 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.Camera' )
  var i3387 = data
  i3386.enabled = !!i3387[0]
  i3386.aspect = i3387[1]
  i3386.orthographic = !!i3387[2]
  i3386.orthographicSize = i3387[3]
  i3386.backgroundColor = new pc.Color(i3387[4], i3387[5], i3387[6], i3387[7])
  i3386.nearClipPlane = i3387[8]
  i3386.farClipPlane = i3387[9]
  i3386.fieldOfView = i3387[10]
  i3386.depth = i3387[11]
  i3386.clearFlags = i3387[12]
  i3386.cullingMask = i3387[13]
  i3386.rect = i3387[14]
  request.r(i3387[15], i3387[16], 0, i3386, 'targetTexture')
  return i3386
}

Deserializers["Luna.Unity.DTO.UnityEngine.Scene.GameObject"] = function (request, data, root) {
  var i3388 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Scene.GameObject' )
  var i3389 = data
  i3388.name = i3389[0]
  i3388.tag = i3389[1]
  i3388.enabled = !!i3389[2]
  i3388.isStatic = !!i3389[3]
  i3388.layer = i3389[4]
  return i3388
}

Deserializers["Luna.Unity.DTO.UnityEngine.Textures.Texture2D"] = function (request, data, root) {
  var i3390 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Textures.Texture2D' )
  var i3391 = data
  i3390.name = i3391[0]
  i3390.width = i3391[1]
  i3390.height = i3391[2]
  i3390.mipmapCount = i3391[3]
  i3390.anisoLevel = i3391[4]
  i3390.filterMode = i3391[5]
  i3390.hdr = !!i3391[6]
  i3390.format = i3391[7]
  i3390.wrapMode = i3391[8]
  i3390.alphaIsTransparency = !!i3391[9]
  i3390.alphaSource = i3391[10]
  return i3390
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Mesh"] = function (request, data, root) {
  var i3392 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Mesh' )
  var i3393 = data
  i3392.name = i3393[0]
  i3392.halfPrecision = !!i3393[1]
  i3392.vertexCount = i3393[2]
  i3392.aabb = i3393[3]
  var i3395 = i3393[4]
  var i3394 = []
  for(var i = 0; i < i3395.length; i += 1) {
    i3394.push( !!i3395[i + 0] );
  }
  i3392.streams = i3394
  i3392.vertices = i3393[5]
  var i3397 = i3393[6]
  var i3396 = []
  for(var i = 0; i < i3397.length; i += 1) {
    i3396.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Mesh+SubMesh', i3397[i + 0]) );
  }
  i3392.subMeshes = i3396
  var i3399 = i3393[7]
  var i3398 = []
  for(var i = 0; i < i3399.length; i += 16) {
    i3398.push( new pc.Mat4().setData(i3399[i + 0], i3399[i + 1], i3399[i + 2], i3399[i + 3],  i3399[i + 4], i3399[i + 5], i3399[i + 6], i3399[i + 7],  i3399[i + 8], i3399[i + 9], i3399[i + 10], i3399[i + 11],  i3399[i + 12], i3399[i + 13], i3399[i + 14], i3399[i + 15]) );
  }
  i3392.bindposes = i3398
  var i3401 = i3393[8]
  var i3400 = []
  for(var i = 0; i < i3401.length; i += 1) {
    i3400.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Mesh+BlendShape', i3401[i + 0]) );
  }
  i3392.blendShapes = i3400
  return i3392
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Mesh+SubMesh"] = function (request, data, root) {
  var i3406 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Mesh+SubMesh' )
  var i3407 = data
  i3406.triangles = i3407[0]
  return i3406
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Mesh+BlendShape"] = function (request, data, root) {
  var i3412 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Mesh+BlendShape' )
  var i3413 = data
  i3412.name = i3413[0]
  var i3415 = i3413[1]
  var i3414 = []
  for(var i = 0; i < i3415.length; i += 1) {
    i3414.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Mesh+BlendShapeFrame', i3415[i + 0]) );
  }
  i3412.frames = i3414
  return i3412
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.RectTransform"] = function (request, data, root) {
  var i3416 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.RectTransform' )
  var i3417 = data
  i3416.pivot = new pc.Vec2( i3417[0], i3417[1] )
  i3416.anchorMin = new pc.Vec2( i3417[2], i3417[3] )
  i3416.anchorMax = new pc.Vec2( i3417[4], i3417[5] )
  i3416.sizeDelta = new pc.Vec2( i3417[6], i3417[7] )
  i3416.anchoredPosition3D = new pc.Vec3( i3417[8], i3417[9], i3417[10] )
  i3416.rotation = new pc.Quat(i3417[11], i3417[12], i3417[13], i3417[14])
  i3416.scale = new pc.Vec3( i3417[15], i3417[16], i3417[17] )
  return i3416
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.Canvas"] = function (request, data, root) {
  var i3418 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.Canvas' )
  var i3419 = data
  i3418.enabled = !!i3419[0]
  i3418.planeDistance = i3419[1]
  i3418.referencePixelsPerUnit = i3419[2]
  i3418.isFallbackOverlay = !!i3419[3]
  i3418.renderMode = i3419[4]
  i3418.renderOrder = i3419[5]
  i3418.sortingLayerName = i3419[6]
  i3418.sortingOrder = i3419[7]
  i3418.scaleFactor = i3419[8]
  request.r(i3419[9], i3419[10], 0, i3418, 'worldCamera')
  i3418.overrideSorting = !!i3419[11]
  i3418.pixelPerfect = !!i3419[12]
  i3418.targetDisplay = i3419[13]
  i3418.overridePixelPerfect = !!i3419[14]
  return i3418
}

Deserializers["UnityEngine.UI.CanvasScaler"] = function (request, data, root) {
  var i3420 = root || request.c( 'UnityEngine.UI.CanvasScaler' )
  var i3421 = data
  i3420.m_UiScaleMode = i3421[0]
  i3420.m_ReferencePixelsPerUnit = i3421[1]
  i3420.m_ScaleFactor = i3421[2]
  i3420.m_ReferenceResolution = new pc.Vec2( i3421[3], i3421[4] )
  i3420.m_ScreenMatchMode = i3421[5]
  i3420.m_MatchWidthOrHeight = i3421[6]
  i3420.m_PhysicalUnit = i3421[7]
  i3420.m_FallbackScreenDPI = i3421[8]
  i3420.m_DefaultSpriteDPI = i3421[9]
  i3420.m_DynamicPixelsPerUnit = i3421[10]
  i3420.m_PresetInfoIsWorld = !!i3421[11]
  return i3420
}

Deserializers["UnityEngine.UI.GraphicRaycaster"] = function (request, data, root) {
  var i3422 = root || request.c( 'UnityEngine.UI.GraphicRaycaster' )
  var i3423 = data
  i3422.m_IgnoreReversedGraphics = !!i3423[0]
  i3422.m_BlockingObjects = i3423[1]
  i3422.m_BlockingMask = UnityEngine.LayerMask.FromIntegerValue( i3423[2] )
  return i3422
}

Deserializers["UnityEngine.EventSystems.EventTrigger"] = function (request, data, root) {
  var i3424 = root || request.c( 'UnityEngine.EventSystems.EventTrigger' )
  var i3425 = data
  var i3427 = i3425[0]
  var i3426 = new (System.Collections.Generic.List$1(Bridge.ns('UnityEngine.EventSystems.EventTrigger+Entry')))
  for(var i = 0; i < i3427.length; i += 1) {
    i3426.add(request.d('UnityEngine.EventSystems.EventTrigger+Entry', i3427[i + 0]));
  }
  i3424.m_Delegates = i3426
  return i3424
}

Deserializers["UnityEngine.EventSystems.EventTrigger+Entry"] = function (request, data, root) {
  var i3430 = root || request.c( 'UnityEngine.EventSystems.EventTrigger+Entry' )
  var i3431 = data
  i3430.eventID = i3431[0]
  i3430.callback = request.d('UnityEngine.EventSystems.EventTrigger+TriggerEvent', i3431[1], i3430.callback)
  return i3430
}

Deserializers["UnityEngine.EventSystems.EventTrigger+TriggerEvent"] = function (request, data, root) {
  var i3432 = root || request.c( 'UnityEngine.EventSystems.EventTrigger+TriggerEvent' )
  var i3433 = data
  i3432.m_PersistentCalls = request.d('UnityEngine.Events.PersistentCallGroup', i3433[0], i3432.m_PersistentCalls)
  return i3432
}

Deserializers["UnityEngine.Events.PersistentCallGroup"] = function (request, data, root) {
  var i3434 = root || request.c( 'UnityEngine.Events.PersistentCallGroup' )
  var i3435 = data
  var i3437 = i3435[0]
  var i3436 = new (System.Collections.Generic.List$1(Bridge.ns('UnityEngine.Events.PersistentCall')))
  for(var i = 0; i < i3437.length; i += 1) {
    i3436.add(request.d('UnityEngine.Events.PersistentCall', i3437[i + 0]));
  }
  i3434.m_Calls = i3436
  return i3434
}

Deserializers["UnityEngine.Events.PersistentCall"] = function (request, data, root) {
  var i3440 = root || request.c( 'UnityEngine.Events.PersistentCall' )
  var i3441 = data
  request.r(i3441[0], i3441[1], 0, i3440, 'm_Target')
  i3440.m_TargetAssemblyTypeName = i3441[2]
  i3440.m_MethodName = i3441[3]
  i3440.m_Mode = i3441[4]
  i3440.m_Arguments = request.d('UnityEngine.Events.ArgumentCache', i3441[5], i3440.m_Arguments)
  i3440.m_CallState = i3441[6]
  return i3440
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.CanvasRenderer"] = function (request, data, root) {
  var i3442 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.CanvasRenderer' )
  var i3443 = data
  i3442.cullTransparentMesh = !!i3443[0]
  return i3442
}

Deserializers["UnityEngine.UI.RawImage"] = function (request, data, root) {
  var i3444 = root || request.c( 'UnityEngine.UI.RawImage' )
  var i3445 = data
  request.r(i3445[0], i3445[1], 0, i3444, 'm_Texture')
  i3444.m_UVRect = UnityEngine.Rect.MinMaxRect(i3445[2], i3445[3], i3445[4], i3445[5])
  request.r(i3445[6], i3445[7], 0, i3444, 'm_Material')
  i3444.m_Maskable = !!i3445[8]
  i3444.m_Color = new pc.Color(i3445[9], i3445[10], i3445[11], i3445[12])
  i3444.m_RaycastTarget = !!i3445[13]
  i3444.m_RaycastPadding = new pc.Vec4( i3445[14], i3445[15], i3445[16], i3445[17] )
  return i3444
}

Deserializers["UnityEngine.UI.Image"] = function (request, data, root) {
  var i3446 = root || request.c( 'UnityEngine.UI.Image' )
  var i3447 = data
  request.r(i3447[0], i3447[1], 0, i3446, 'm_Sprite')
  i3446.m_Type = i3447[2]
  i3446.m_PreserveAspect = !!i3447[3]
  i3446.m_FillCenter = !!i3447[4]
  i3446.m_FillMethod = i3447[5]
  i3446.m_FillAmount = i3447[6]
  i3446.m_FillClockwise = !!i3447[7]
  i3446.m_FillOrigin = i3447[8]
  i3446.m_UseSpriteMesh = !!i3447[9]
  i3446.m_PixelsPerUnitMultiplier = i3447[10]
  request.r(i3447[11], i3447[12], 0, i3446, 'm_Material')
  i3446.m_Maskable = !!i3447[13]
  i3446.m_Color = new pc.Color(i3447[14], i3447[15], i3447[16], i3447[17])
  i3446.m_RaycastTarget = !!i3447[18]
  i3446.m_RaycastPadding = new pc.Vec4( i3447[19], i3447[20], i3447[21], i3447[22] )
  return i3446
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.Animator"] = function (request, data, root) {
  var i3448 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.Animator' )
  var i3449 = data
  request.r(i3449[0], i3449[1], 0, i3448, 'animatorController')
  i3448.updateMode = i3449[2]
  var i3451 = i3449[3]
  var i3450 = []
  for(var i = 0; i < i3451.length; i += 2) {
  request.r(i3451[i + 0], i3451[i + 1], 2, i3450, '')
  }
  i3448.humanBones = i3450
  i3448.enabled = !!i3449[4]
  return i3448
}

Deserializers["UnityEngine.UI.Button"] = function (request, data, root) {
  var i3454 = root || request.c( 'UnityEngine.UI.Button' )
  var i3455 = data
  i3454.m_OnClick = request.d('UnityEngine.UI.Button+ButtonClickedEvent', i3455[0], i3454.m_OnClick)
  i3454.m_Navigation = request.d('UnityEngine.UI.Navigation', i3455[1], i3454.m_Navigation)
  i3454.m_Transition = i3455[2]
  i3454.m_Colors = request.d('UnityEngine.UI.ColorBlock', i3455[3], i3454.m_Colors)
  i3454.m_SpriteState = request.d('UnityEngine.UI.SpriteState', i3455[4], i3454.m_SpriteState)
  i3454.m_AnimationTriggers = request.d('UnityEngine.UI.AnimationTriggers', i3455[5], i3454.m_AnimationTriggers)
  i3454.m_Interactable = !!i3455[6]
  request.r(i3455[7], i3455[8], 0, i3454, 'm_TargetGraphic')
  return i3454
}

Deserializers["UnityEngine.UI.Button+ButtonClickedEvent"] = function (request, data, root) {
  var i3456 = root || request.c( 'UnityEngine.UI.Button+ButtonClickedEvent' )
  var i3457 = data
  i3456.m_PersistentCalls = request.d('UnityEngine.Events.PersistentCallGroup', i3457[0], i3456.m_PersistentCalls)
  return i3456
}

Deserializers["UnityEngine.Events.ArgumentCache"] = function (request, data, root) {
  var i3458 = root || request.c( 'UnityEngine.Events.ArgumentCache' )
  var i3459 = data
  request.r(i3459[0], i3459[1], 0, i3458, 'm_ObjectArgument')
  i3458.m_ObjectArgumentAssemblyTypeName = i3459[2]
  i3458.m_IntArgument = i3459[3]
  i3458.m_FloatArgument = i3459[4]
  i3458.m_StringArgument = i3459[5]
  i3458.m_BoolArgument = !!i3459[6]
  return i3458
}

Deserializers["UnityEngine.UI.Navigation"] = function (request, data, root) {
  var i3460 = root || request.c( 'UnityEngine.UI.Navigation' )
  var i3461 = data
  i3460.m_Mode = i3461[0]
  i3460.m_WrapAround = !!i3461[1]
  request.r(i3461[2], i3461[3], 0, i3460, 'm_SelectOnUp')
  request.r(i3461[4], i3461[5], 0, i3460, 'm_SelectOnDown')
  request.r(i3461[6], i3461[7], 0, i3460, 'm_SelectOnLeft')
  request.r(i3461[8], i3461[9], 0, i3460, 'm_SelectOnRight')
  return i3460
}

Deserializers["UnityEngine.UI.ColorBlock"] = function (request, data, root) {
  var i3462 = root || request.c( 'UnityEngine.UI.ColorBlock' )
  var i3463 = data
  i3462.m_NormalColor = new pc.Color(i3463[0], i3463[1], i3463[2], i3463[3])
  i3462.m_HighlightedColor = new pc.Color(i3463[4], i3463[5], i3463[6], i3463[7])
  i3462.m_PressedColor = new pc.Color(i3463[8], i3463[9], i3463[10], i3463[11])
  i3462.m_SelectedColor = new pc.Color(i3463[12], i3463[13], i3463[14], i3463[15])
  i3462.m_DisabledColor = new pc.Color(i3463[16], i3463[17], i3463[18], i3463[19])
  i3462.m_ColorMultiplier = i3463[20]
  i3462.m_FadeDuration = i3463[21]
  return i3462
}

Deserializers["UnityEngine.UI.SpriteState"] = function (request, data, root) {
  var i3464 = root || request.c( 'UnityEngine.UI.SpriteState' )
  var i3465 = data
  request.r(i3465[0], i3465[1], 0, i3464, 'm_HighlightedSprite')
  request.r(i3465[2], i3465[3], 0, i3464, 'm_PressedSprite')
  request.r(i3465[4], i3465[5], 0, i3464, 'm_SelectedSprite')
  request.r(i3465[6], i3465[7], 0, i3464, 'm_DisabledSprite')
  return i3464
}

Deserializers["UnityEngine.UI.AnimationTriggers"] = function (request, data, root) {
  var i3466 = root || request.c( 'UnityEngine.UI.AnimationTriggers' )
  var i3467 = data
  i3466.m_NormalTrigger = i3467[0]
  i3466.m_HighlightedTrigger = i3467[1]
  i3466.m_PressedTrigger = i3467[2]
  i3466.m_SelectedTrigger = i3467[3]
  i3466.m_DisabledTrigger = i3467[4]
  return i3466
}

Deserializers["UnityEngine.UI.AspectRatioFitter"] = function (request, data, root) {
  var i3468 = root || request.c( 'UnityEngine.UI.AspectRatioFitter' )
  var i3469 = data
  i3468.m_AspectMode = i3469[0]
  i3468.m_AspectRatio = i3469[1]
  return i3468
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.SpriteRenderer"] = function (request, data, root) {
  var i3470 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.SpriteRenderer' )
  var i3471 = data
  i3470.enabled = !!i3471[0]
  request.r(i3471[1], i3471[2], 0, i3470, 'sharedMaterial')
  var i3473 = i3471[3]
  var i3472 = []
  for(var i = 0; i < i3473.length; i += 2) {
  request.r(i3473[i + 0], i3473[i + 1], 2, i3472, '')
  }
  i3470.sharedMaterials = i3472
  i3470.receiveShadows = !!i3471[4]
  i3470.shadowCastingMode = i3471[5]
  i3470.sortingLayerID = i3471[6]
  i3470.sortingOrder = i3471[7]
  i3470.lightmapIndex = i3471[8]
  i3470.lightmapSceneIndex = i3471[9]
  i3470.lightmapScaleOffset = new pc.Vec4( i3471[10], i3471[11], i3471[12], i3471[13] )
  i3470.lightProbeUsage = i3471[14]
  i3470.reflectionProbeUsage = i3471[15]
  i3470.color = new pc.Color(i3471[16], i3471[17], i3471[18], i3471[19])
  request.r(i3471[20], i3471[21], 0, i3470, 'sprite')
  i3470.flipX = !!i3471[22]
  i3470.flipY = !!i3471[23]
  i3470.drawMode = i3471[24]
  i3470.size = new pc.Vec2( i3471[25], i3471[26] )
  i3470.tileMode = i3471[27]
  i3470.adaptiveModeThreshold = i3471[28]
  i3470.maskInteraction = i3471[29]
  i3470.spriteSortPoint = i3471[30]
  return i3470
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.CapsuleCollider"] = function (request, data, root) {
  var i3476 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.CapsuleCollider' )
  var i3477 = data
  i3476.center = new pc.Vec3( i3477[0], i3477[1], i3477[2] )
  i3476.radius = i3477[3]
  i3476.height = i3477[4]
  i3476.direction = i3477[5]
  i3476.enabled = !!i3477[6]
  i3476.isTrigger = !!i3477[7]
  request.r(i3477[8], i3477[9], 0, i3476, 'material')
  return i3476
}

Deserializers["GameManager"] = function (request, data, root) {
  var i3478 = root || request.c( 'GameManager' )
  var i3479 = data
  request.r(i3479[0], i3479[1], 0, i3478, 'UIElems')
  request.r(i3479[2], i3479[3], 0, i3478, 'bars')
  request.r(i3479[4], i3479[5], 0, i3478, 'endUI')
  request.r(i3479[6], i3479[7], 0, i3478, 'CTA')
  request.r(i3479[8], i3479[9], 0, i3478, 'video')
  i3478.isContinue = !!i3479[10]
  i3478.alwaysFail = !!i3479[11]
  i3478.nextFail = !!i3479[12]
  i3478.isShootFail = !!i3479[13]
  request.r(i3479[14], i3479[15], 0, i3478, 'normClipLandscape')
  request.r(i3479[16], i3479[17], 0, i3478, 'failClipLandscape')
  request.r(i3479[18], i3479[19], 0, i3478, 'shootFailClipLandscape')
  request.r(i3479[20], i3479[21], 0, i3478, 'normClipPortrait')
  request.r(i3479[22], i3479[23], 0, i3478, 'failClipPortrait')
  request.r(i3479[24], i3479[25], 0, i3478, 'shootFailClipPortrait')
  request.r(i3479[26], i3479[27], 0, i3478, 'HorizontalCollider')
  request.r(i3479[28], i3479[29], 0, i3478, 'VerticalCollider')
  request.r(i3479[30], i3479[31], 0, i3478, 'timeBar')
  i3478.timeLimit = i3479[32]
  return i3478
}

Deserializers["TouchInputManager"] = function (request, data, root) {
  var i3480 = root || request.c( 'TouchInputManager' )
  var i3481 = data
  request.r(i3481[0], i3481[1], 0, i3480, 'Scope')
  request.r(i3481[2], i3481[3], 0, i3480, 'text')
  request.r(i3481[4], i3481[5], 0, i3480, 'shootButton')
  request.r(i3481[6], i3481[7], 0, i3480, 'trigger')
  return i3480
}

Deserializers["UnityEngine.EventSystems.EventSystem"] = function (request, data, root) {
  var i3482 = root || request.c( 'UnityEngine.EventSystems.EventSystem' )
  var i3483 = data
  request.r(i3483[0], i3483[1], 0, i3482, 'm_FirstSelected')
  i3482.m_sendNavigationEvents = !!i3483[2]
  i3482.m_DragThreshold = i3483[3]
  return i3482
}

Deserializers["UnityEngine.EventSystems.StandaloneInputModule"] = function (request, data, root) {
  var i3484 = root || request.c( 'UnityEngine.EventSystems.StandaloneInputModule' )
  var i3485 = data
  i3484.m_HorizontalAxis = i3485[0]
  i3484.m_VerticalAxis = i3485[1]
  i3484.m_SubmitButton = i3485[2]
  i3484.m_CancelButton = i3485[3]
  i3484.m_InputActionsPerSecond = i3485[4]
  i3484.m_RepeatDelay = i3485[5]
  i3484.m_ForceModuleActive = !!i3485[6]
  return i3484
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.MeshFilter"] = function (request, data, root) {
  var i3486 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.MeshFilter' )
  var i3487 = data
  request.r(i3487[0], i3487[1], 0, i3486, 'sharedMesh')
  return i3486
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.MeshRenderer"] = function (request, data, root) {
  var i3488 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.MeshRenderer' )
  var i3489 = data
  request.r(i3489[0], i3489[1], 0, i3488, 'additionalVertexStreams')
  i3488.enabled = !!i3489[2]
  request.r(i3489[3], i3489[4], 0, i3488, 'sharedMaterial')
  var i3491 = i3489[5]
  var i3490 = []
  for(var i = 0; i < i3491.length; i += 2) {
  request.r(i3491[i + 0], i3491[i + 1], 2, i3490, '')
  }
  i3488.sharedMaterials = i3490
  i3488.receiveShadows = !!i3489[6]
  i3488.shadowCastingMode = i3489[7]
  i3488.sortingLayerID = i3489[8]
  i3488.sortingOrder = i3489[9]
  i3488.lightmapIndex = i3489[10]
  i3488.lightmapSceneIndex = i3489[11]
  i3488.lightmapScaleOffset = new pc.Vec4( i3489[12], i3489[13], i3489[14], i3489[15] )
  i3488.lightProbeUsage = i3489[16]
  i3488.reflectionProbeUsage = i3489[17]
  return i3488
}

Deserializers["Luna.Unity.DTO.UnityEngine.Components.VideoPlayer"] = function (request, data, root) {
  var i3492 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Components.VideoPlayer' )
  var i3493 = data
  i3492.enabled = !!i3493[0]
  i3492.source = i3493[1]
  request.r(i3493[2], i3493[3], 0, i3492, 'clip')
  i3492.url = i3493[4]
  i3492.playOnAwake = !!i3493[5]
  i3492.isLooping = !!i3493[6]
  i3492.renderMode = i3493[7]
  request.r(i3493[8], i3493[9], 0, i3492, 'targetMaterialRenderer')
  i3492.targetMaterialProperty = i3493[10]
  i3492.playbackSpeed = i3493[11]
  return i3492
}

Deserializers["Luna.Unity.DTO.UnityEngine.Textures.Cubemap"] = function (request, data, root) {
  var i3494 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Textures.Cubemap' )
  var i3495 = data
  i3494.name = i3495[0]
  i3494.atlasId = i3495[1]
  i3494.mipmapCount = i3495[2]
  i3494.hdr = !!i3495[3]
  i3494.size = i3495[4]
  i3494.anisoLevel = i3495[5]
  i3494.filterMode = i3495[6]
  i3494.wrapMode = i3495[7]
  var i3497 = i3495[8]
  var i3496 = []
  for(var i = 0; i < i3497.length; i += 4) {
    i3496.push( UnityEngine.Rect.MinMaxRect(i3497[i + 0], i3497[i + 1], i3497[i + 2], i3497[i + 3]) );
  }
  i3494.rects = i3496
  return i3494
}

Deserializers["Luna.Unity.DTO.UnityEngine.Scene.Scene"] = function (request, data, root) {
  var i3500 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Scene.Scene' )
  var i3501 = data
  i3500.name = i3501[0]
  i3500.index = i3501[1]
  i3500.startup = !!i3501[2]
  return i3500
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.RenderSettings"] = function (request, data, root) {
  var i3502 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.RenderSettings' )
  var i3503 = data
  i3502.ambientIntensity = i3503[0]
  i3502.reflectionIntensity = i3503[1]
  i3502.ambientMode = i3503[2]
  i3502.ambientLight = new pc.Color(i3503[3], i3503[4], i3503[5], i3503[6])
  i3502.ambientSkyColor = new pc.Color(i3503[7], i3503[8], i3503[9], i3503[10])
  i3502.ambientGroundColor = new pc.Color(i3503[11], i3503[12], i3503[13], i3503[14])
  i3502.ambientEquatorColor = new pc.Color(i3503[15], i3503[16], i3503[17], i3503[18])
  i3502.fogColor = new pc.Color(i3503[19], i3503[20], i3503[21], i3503[22])
  i3502.fogEndDistance = i3503[23]
  i3502.fogStartDistance = i3503[24]
  i3502.fogDensity = i3503[25]
  i3502.fog = !!i3503[26]
  request.r(i3503[27], i3503[28], 0, i3502, 'skybox')
  i3502.fogMode = i3503[29]
  var i3505 = i3503[30]
  var i3504 = []
  for(var i = 0; i < i3505.length; i += 1) {
    i3504.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.RenderSettings+Lightmap', i3505[i + 0]) );
  }
  i3502.lightmaps = i3504
  i3502.lightProbes = request.d('Luna.Unity.DTO.UnityEngine.Assets.RenderSettings+LightProbes', i3503[31], i3502.lightProbes)
  i3502.lightmapsMode = i3503[32]
  i3502.environmentLightingMode = i3503[33]
  i3502.ambientProbe = new pc.SphericalHarmonicsL2(i3503[34])
  request.r(i3503[35], i3503[36], 0, i3502, 'customReflection')
  request.r(i3503[37], i3503[38], 0, i3502, 'defaultReflection')
  i3502.defaultReflectionMode = i3503[39]
  i3502.defaultReflectionResolution = i3503[40]
  i3502.sunLightObjectId = i3503[41]
  i3502.pixelLightCount = i3503[42]
  i3502.defaultReflectionHDR = !!i3503[43]
  i3502.hasLightDataAsset = !!i3503[44]
  i3502.hasManualGenerate = !!i3503[45]
  return i3502
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.RenderSettings+Lightmap"] = function (request, data, root) {
  var i3508 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.RenderSettings+Lightmap' )
  var i3509 = data
  request.r(i3509[0], i3509[1], 0, i3508, 'lightmapColor')
  request.r(i3509[2], i3509[3], 0, i3508, 'lightmapDirection')
  return i3508
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.RenderSettings+LightProbes"] = function (request, data, root) {
  var i3510 = root || new UnityEngine.LightProbes()
  var i3511 = data
  return i3510
}

Deserializers["DG.Tweening.Core.DOTweenSettings"] = function (request, data, root) {
  var i3518 = root || request.c( 'DG.Tweening.Core.DOTweenSettings' )
  var i3519 = data
  i3518.useSafeMode = !!i3519[0]
  i3518.safeModeOptions = request.d('DG.Tweening.Core.DOTweenSettings+SafeModeOptions', i3519[1], i3518.safeModeOptions)
  i3518.timeScale = i3519[2]
  i3518.useSmoothDeltaTime = !!i3519[3]
  i3518.maxSmoothUnscaledTime = i3519[4]
  i3518.rewindCallbackMode = i3519[5]
  i3518.showUnityEditorReport = !!i3519[6]
  i3518.logBehaviour = i3519[7]
  i3518.drawGizmos = !!i3519[8]
  i3518.defaultRecyclable = !!i3519[9]
  i3518.defaultAutoPlay = i3519[10]
  i3518.defaultUpdateType = i3519[11]
  i3518.defaultTimeScaleIndependent = !!i3519[12]
  i3518.defaultEaseType = i3519[13]
  i3518.defaultEaseOvershootOrAmplitude = i3519[14]
  i3518.defaultEasePeriod = i3519[15]
  i3518.defaultAutoKill = !!i3519[16]
  i3518.defaultLoopType = i3519[17]
  i3518.debugMode = !!i3519[18]
  i3518.debugStoreTargetId = !!i3519[19]
  i3518.showPreviewPanel = !!i3519[20]
  i3518.storeSettingsLocation = i3519[21]
  i3518.modules = request.d('DG.Tweening.Core.DOTweenSettings+ModulesSetup', i3519[22], i3518.modules)
  i3518.createASMDEF = !!i3519[23]
  i3518.showPlayingTweens = !!i3519[24]
  i3518.showPausedTweens = !!i3519[25]
  return i3518
}

Deserializers["DG.Tweening.Core.DOTweenSettings+SafeModeOptions"] = function (request, data, root) {
  var i3520 = root || request.c( 'DG.Tweening.Core.DOTweenSettings+SafeModeOptions' )
  var i3521 = data
  i3520.logBehaviour = i3521[0]
  i3520.nestedTweenFailureBehaviour = i3521[1]
  return i3520
}

Deserializers["DG.Tweening.Core.DOTweenSettings+ModulesSetup"] = function (request, data, root) {
  var i3522 = root || request.c( 'DG.Tweening.Core.DOTweenSettings+ModulesSetup' )
  var i3523 = data
  i3522.showPanel = !!i3523[0]
  i3522.audioEnabled = !!i3523[1]
  i3522.physicsEnabled = !!i3523[2]
  i3522.physics2DEnabled = !!i3523[3]
  i3522.spriteEnabled = !!i3523[4]
  i3522.uiEnabled = !!i3523[5]
  i3522.textMeshProEnabled = !!i3523[6]
  i3522.tk2DEnabled = !!i3523[7]
  i3522.deAudioEnabled = !!i3523[8]
  i3522.deUnityExtendedEnabled = !!i3523[9]
  i3522.epoOutlineEnabled = !!i3523[10]
  return i3522
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Shader"] = function (request, data, root) {
  var i3524 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Shader' )
  var i3525 = data
  var i3527 = i3525[0]
  var i3526 = new (System.Collections.Generic.List$1(Bridge.ns('System.String')))
  for(var i = 0; i < i3527.length; i += 1) {
    i3526.add(i3527[i + 0]);
  }
  i3524.invalidShaderVariants = i3526
  i3524.name = i3525[1]
  var i3529 = i3525[2]
  var i3528 = []
  for(var i = 0; i < i3529.length; i += 1) {
    i3528.push( i3529[i + 0] );
  }
  i3524.shaderDefinedKeywords = i3528
  var i3531 = i3525[3]
  var i3530 = []
  for(var i = 0; i < i3531.length; i += 1) {
    i3530.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass', i3531[i + 0]) );
  }
  i3524.passes = i3530
  var i3533 = i3525[4]
  var i3532 = []
  for(var i = 0; i < i3533.length; i += 1) {
    i3532.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+DefaultParameterValue', i3533[i + 0]) );
  }
  i3524.defaultParameterValues = i3532
  request.r(i3525[5], i3525[6], 0, i3524, 'unityFallbackShader')
  i3524.readDepth = !!i3525[7]
  i3524.IsCreatedByShaderGraph = !!i3525[8]
  return i3524
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass"] = function (request, data, root) {
  var i3540 = root || new pc.UnityShaderPass()
  var i3541 = data
  i3540.passType = i3541[0]
  i3540.zTest = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3541[1], i3540.zTest)
  i3540.zWrite = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3541[2], i3540.zWrite)
  i3540.culling = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3541[3], i3540.culling)
  i3540.blending = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Blending', i3541[4], i3540.blending)
  i3540.alphaBlending = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Blending', i3541[5], i3540.alphaBlending)
  i3540.colorWriteMask = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3541[6], i3540.colorWriteMask)
  i3540.offsetUnits = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3541[7], i3540.offsetUnits)
  i3540.offsetFactor = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3541[8], i3540.offsetFactor)
  i3540.stencilRef = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3541[9], i3540.stencilRef)
  i3540.stencilReadMask = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3541[10], i3540.stencilReadMask)
  i3540.stencilWriteMask = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3541[11], i3540.stencilWriteMask)
  i3540.stencilOp = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+StencilOp', i3541[12], i3540.stencilOp)
  i3540.stencilOpFront = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+StencilOp', i3541[13], i3540.stencilOpFront)
  i3540.stencilOpBack = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+StencilOp', i3541[14], i3540.stencilOpBack)
  var i3543 = i3541[15]
  var i3542 = []
  for(var i = 0; i < i3543.length; i += 1) {
    i3542.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Tag', i3543[i + 0]) );
  }
  i3540.tags = i3542
  var i3545 = i3541[16]
  var i3544 = []
  for(var i = 0; i < i3545.length; i += 1) {
    i3544.push( i3545[i + 0] );
  }
  i3540.passDefinedKeywords = i3544
  var i3547 = i3541[17]
  var i3546 = []
  for(var i = 0; i < i3547.length; i += 1) {
    i3546.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Variant', i3547[i + 0]) );
  }
  i3540.variants = i3546
  i3540.readDepth = !!i3541[18]
  return i3540
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value"] = function (request, data, root) {
  var i3548 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value' )
  var i3549 = data
  i3548.val = i3549[0]
  i3548.name = i3549[1]
  return i3548
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Blending"] = function (request, data, root) {
  var i3550 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Blending' )
  var i3551 = data
  i3550.src = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3551[0], i3550.src)
  i3550.dst = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3551[1], i3550.dst)
  i3550.op = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3551[2], i3550.op)
  return i3550
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+StencilOp"] = function (request, data, root) {
  var i3552 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+StencilOp' )
  var i3553 = data
  i3552.pass = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3553[0], i3552.pass)
  i3552.fail = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3553[1], i3552.fail)
  i3552.zFail = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3553[2], i3552.zFail)
  i3552.comp = request.d('Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value', i3553[3], i3552.comp)
  return i3552
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Tag"] = function (request, data, root) {
  var i3556 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Tag' )
  var i3557 = data
  i3556.name = i3557[0]
  i3556.value = i3557[1]
  return i3556
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Variant"] = function (request, data, root) {
  var i3560 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Variant' )
  var i3561 = data
  var i3563 = i3561[0]
  var i3562 = []
  for(var i = 0; i < i3563.length; i += 1) {
    i3562.push( i3563[i + 0] );
  }
  i3560.keywords = i3562
  i3560.vertexProgram = i3561[1]
  i3560.fragmentProgram = i3561[2]
  i3560.readDepth = !!i3561[3]
  return i3560
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Shader+DefaultParameterValue"] = function (request, data, root) {
  var i3566 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Shader+DefaultParameterValue' )
  var i3567 = data
  i3566.name = i3567[0]
  i3566.type = i3567[1]
  i3566.value = new pc.Vec4( i3567[2], i3567[3], i3567[4], i3567[5] )
  i3566.textureValue = i3567[6]
  return i3566
}

Deserializers["Luna.Unity.DTO.UnityEngine.Textures.Sprite"] = function (request, data, root) {
  var i3568 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Textures.Sprite' )
  var i3569 = data
  i3568.name = i3569[0]
  request.r(i3569[1], i3569[2], 0, i3568, 'texture')
  i3568.aabb = i3569[3]
  i3568.vertices = i3569[4]
  i3568.triangles = i3569[5]
  i3568.textureRect = UnityEngine.Rect.MinMaxRect(i3569[6], i3569[7], i3569[8], i3569[9])
  i3568.packedRect = UnityEngine.Rect.MinMaxRect(i3569[10], i3569[11], i3569[12], i3569[13])
  i3568.border = new pc.Vec4( i3569[14], i3569[15], i3569[16], i3569[17] )
  i3568.transparency = i3569[18]
  i3568.bounds = i3569[19]
  i3568.pixelsPerUnit = i3569[20]
  i3568.textureWidth = i3569[21]
  i3568.textureHeight = i3569[22]
  i3568.nativeSize = new pc.Vec2( i3569[23], i3569[24] )
  i3568.pivot = new pc.Vec2( i3569[25], i3569[26] )
  i3568.textureRectOffset = new pc.Vec2( i3569[27], i3569[28] )
  return i3568
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.VideoClip"] = function (request, data, root) {
  var i3570 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.VideoClip' )
  var i3571 = data
  i3570.name = i3571[0]
  i3570.width = i3571[1]
  i3570.height = i3571[2]
  i3570.frameRate = i3571[3]
  i3570.frameCount = System.UInt64(i3571[4])
  return i3570
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationClip"] = function (request, data, root) {
  var i3572 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationClip' )
  var i3573 = data
  i3572.name = i3573[0]
  i3572.wrapMode = i3573[1]
  i3572.isLooping = !!i3573[2]
  i3572.length = i3573[3]
  var i3575 = i3573[4]
  var i3574 = []
  for(var i = 0; i < i3575.length; i += 1) {
    i3574.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationCurve', i3575[i + 0]) );
  }
  i3572.curves = i3574
  var i3577 = i3573[5]
  var i3576 = []
  for(var i = 0; i < i3577.length; i += 1) {
    i3576.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationEvent', i3577[i + 0]) );
  }
  i3572.events = i3576
  i3572.halfPrecision = !!i3573[6]
  return i3572
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationCurve"] = function (request, data, root) {
  var i3580 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationCurve' )
  var i3581 = data
  i3580.path = i3581[0]
  i3580.componentType = i3581[1]
  i3580.property = i3581[2]
  i3580.keys = i3581[3]
  var i3583 = i3581[4]
  var i3582 = []
  for(var i = 0; i < i3583.length; i += 1) {
    i3582.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationCurve+ObjectReferenceKey', i3583[i + 0]) );
  }
  i3580.objectReferenceKeys = i3582
  return i3580
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationCurve+ObjectReferenceKey"] = function (request, data, root) {
  var i3586 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationCurve+ObjectReferenceKey' )
  var i3587 = data
  i3586.time = i3587[0]
  request.r(i3587[1], i3587[2], 0, i3586, 'value')
  return i3586
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationEvent"] = function (request, data, root) {
  var i3590 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationEvent' )
  var i3591 = data
  i3590.functionName = i3591[0]
  i3590.floatParameter = i3591[1]
  i3590.intParameter = i3591[2]
  i3590.stringParameter = i3591[3]
  request.r(i3591[4], i3591[5], 0, i3590, 'objectReferenceParameter')
  i3590.time = i3591[6]
  return i3590
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorController"] = function (request, data, root) {
  var i3592 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorController' )
  var i3593 = data
  i3592.name = i3593[0]
  var i3595 = i3593[1]
  var i3594 = []
  for(var i = 0; i < i3595.length; i += 1) {
    i3594.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorState', i3595[i + 0]) );
  }
  i3592.states = i3594
  var i3597 = i3593[2]
  var i3596 = []
  for(var i = 0; i < i3597.length; i += 1) {
    i3596.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorControllerLayer', i3597[i + 0]) );
  }
  i3592.layers = i3596
  var i3599 = i3593[3]
  var i3598 = []
  for(var i = 0; i < i3599.length; i += 1) {
    i3598.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorControllerParameter', i3599[i + 0]) );
  }
  i3592.parameters = i3598
  var i3601 = i3593[4]
  var i3600 = []
  for(var i = 0; i < i3601.length; i += 1) {
    i3600.push( i3601[i + 0] );
  }
  i3592.animationClips = i3600
  return i3592
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorState"] = function (request, data, root) {
  var i3604 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorState' )
  var i3605 = data
  i3604.cycleOffset = i3605[0]
  i3604.cycleOffsetParameter = i3605[1]
  i3604.cycleOffsetParameterActive = !!i3605[2]
  i3604.mirror = !!i3605[3]
  i3604.mirrorParameter = i3605[4]
  i3604.mirrorParameterActive = !!i3605[5]
  i3604.motionId = i3605[6]
  i3604.nameHash = i3605[7]
  i3604.fullPathHash = i3605[8]
  i3604.speed = i3605[9]
  i3604.speedParameter = i3605[10]
  i3604.speedParameterActive = !!i3605[11]
  i3604.tag = i3605[12]
  i3604.name = i3605[13]
  i3604.writeDefaultValues = !!i3605[14]
  var i3607 = i3605[15]
  var i3606 = []
  for(var i = 0; i < i3607.length; i += 1) {
    i3606.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorStateTransition', i3607[i + 0]) );
  }
  i3604.transitions = i3606
  var i3609 = i3605[16]
  var i3608 = []
  for(var i = 0; i < i3609.length; i += 2) {
  request.r(i3609[i + 0], i3609[i + 1], 2, i3608, '')
  }
  i3604.behaviours = i3608
  return i3604
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorStateTransition"] = function (request, data, root) {
  var i3612 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorStateTransition' )
  var i3613 = data
  i3612.fullPath = i3613[0]
  i3612.canTransitionToSelf = !!i3613[1]
  i3612.duration = i3613[2]
  i3612.exitTime = i3613[3]
  i3612.hasExitTime = !!i3613[4]
  i3612.hasFixedDuration = !!i3613[5]
  i3612.interruptionSource = i3613[6]
  i3612.offset = i3613[7]
  i3612.orderedInterruption = !!i3613[8]
  i3612.destinationStateNameHash = i3613[9]
  i3612.destinationStateMachineId = i3613[10]
  i3612.isExit = !!i3613[11]
  i3612.mute = !!i3613[12]
  i3612.solo = !!i3613[13]
  var i3615 = i3613[14]
  var i3614 = []
  for(var i = 0; i < i3615.length; i += 1) {
    i3614.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorCondition', i3615[i + 0]) );
  }
  i3612.conditions = i3614
  return i3612
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorControllerLayer"] = function (request, data, root) {
  var i3620 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorControllerLayer' )
  var i3621 = data
  i3620.blendingMode = i3621[0]
  i3620.defaultWeight = i3621[1]
  i3620.name = i3621[2]
  i3620.stateMachine = request.d('Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorStateMachine', i3621[3], i3620.stateMachine)
  return i3620
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorStateMachine"] = function (request, data, root) {
  var i3622 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorStateMachine' )
  var i3623 = data
  i3622.id = i3623[0]
  i3622.defaultStateNameHash = i3623[1]
  var i3625 = i3623[2]
  var i3624 = []
  for(var i = 0; i < i3625.length; i += 1) {
    i3624.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorTransition', i3625[i + 0]) );
  }
  i3622.entryTransitions = i3624
  var i3627 = i3623[3]
  var i3626 = []
  for(var i = 0; i < i3627.length; i += 1) {
    i3626.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorStateTransition', i3627[i + 0]) );
  }
  i3622.anyStateTransitions = i3626
  return i3622
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorTransition"] = function (request, data, root) {
  var i3630 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorTransition' )
  var i3631 = data
  i3630.destinationStateNameHash = i3631[0]
  i3630.destinationStateMachineId = i3631[1]
  i3630.isExit = !!i3631[2]
  i3630.mute = !!i3631[3]
  i3630.solo = !!i3631[4]
  var i3633 = i3631[5]
  var i3632 = []
  for(var i = 0; i < i3633.length; i += 1) {
    i3632.push( request.d('Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorCondition', i3633[i + 0]) );
  }
  i3630.conditions = i3632
  return i3630
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorCondition"] = function (request, data, root) {
  var i3636 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorCondition' )
  var i3637 = data
  i3636.mode = i3637[0]
  i3636.parameter = i3637[1]
  i3636.threshold = i3637[2]
  return i3636
}

Deserializers["Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorControllerParameter"] = function (request, data, root) {
  var i3640 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorControllerParameter' )
  var i3641 = data
  i3640.defaultBool = !!i3641[0]
  i3640.defaultFloat = i3641[1]
  i3640.defaultInt = i3641[2]
  i3640.name = i3641[3]
  i3640.nameHash = i3641[4]
  i3640.type = i3641[5]
  return i3640
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Resources"] = function (request, data, root) {
  var i3642 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Resources' )
  var i3643 = data
  var i3645 = i3643[0]
  var i3644 = []
  for(var i = 0; i < i3645.length; i += 1) {
    i3644.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.Resources+File', i3645[i + 0]) );
  }
  i3642.files = i3644
  i3642.componentToPrefabIds = i3643[1]
  return i3642
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Resources+File"] = function (request, data, root) {
  var i3648 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Resources+File' )
  var i3649 = data
  i3648.path = i3649[0]
  request.r(i3649[1], i3649[2], 0, i3648, 'unityObject')
  return i3648
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings"] = function (request, data, root) {
  var i3650 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings' )
  var i3651 = data
  var i3653 = i3651[0]
  var i3652 = []
  for(var i = 0; i < i3653.length; i += 1) {
    i3652.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+ScriptsExecutionOrder', i3653[i + 0]) );
  }
  i3650.scriptsExecutionOrder = i3652
  var i3655 = i3651[1]
  var i3654 = []
  for(var i = 0; i < i3655.length; i += 1) {
    i3654.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+SortingLayer', i3655[i + 0]) );
  }
  i3650.sortingLayers = i3654
  var i3657 = i3651[2]
  var i3656 = []
  for(var i = 0; i < i3657.length; i += 1) {
    i3656.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+CullingLayer', i3657[i + 0]) );
  }
  i3650.cullingLayers = i3656
  i3650.timeSettings = request.d('Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+TimeSettings', i3651[3], i3650.timeSettings)
  i3650.physicsSettings = request.d('Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+PhysicsSettings', i3651[4], i3650.physicsSettings)
  i3650.physics2DSettings = request.d('Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+Physics2DSettings', i3651[5], i3650.physics2DSettings)
  i3650.qualitySettings = request.d('Luna.Unity.DTO.UnityEngine.Assets.QualitySettings', i3651[6], i3650.qualitySettings)
  i3650.removeShadows = !!i3651[7]
  i3650.autoInstantiatePrefabs = !!i3651[8]
  i3650.enableAutoInstancing = !!i3651[9]
  i3650.lightmapEncodingQuality = i3651[10]
  i3650.desiredColorSpace = i3651[11]
  return i3650
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+ScriptsExecutionOrder"] = function (request, data, root) {
  var i3660 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+ScriptsExecutionOrder' )
  var i3661 = data
  i3660.name = i3661[0]
  i3660.value = i3661[1]
  return i3660
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+SortingLayer"] = function (request, data, root) {
  var i3664 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+SortingLayer' )
  var i3665 = data
  i3664.id = i3665[0]
  i3664.name = i3665[1]
  i3664.value = i3665[2]
  return i3664
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+CullingLayer"] = function (request, data, root) {
  var i3668 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+CullingLayer' )
  var i3669 = data
  i3668.id = i3669[0]
  i3668.name = i3669[1]
  return i3668
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+TimeSettings"] = function (request, data, root) {
  var i3670 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+TimeSettings' )
  var i3671 = data
  i3670.fixedDeltaTime = i3671[0]
  i3670.maximumDeltaTime = i3671[1]
  i3670.timeScale = i3671[2]
  i3670.maximumParticleTimestep = i3671[3]
  return i3670
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+PhysicsSettings"] = function (request, data, root) {
  var i3672 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+PhysicsSettings' )
  var i3673 = data
  i3672.gravity = new pc.Vec3( i3673[0], i3673[1], i3673[2] )
  i3672.defaultSolverIterations = i3673[3]
  i3672.bounceThreshold = i3673[4]
  i3672.autoSyncTransforms = !!i3673[5]
  i3672.autoSimulation = !!i3673[6]
  var i3675 = i3673[7]
  var i3674 = []
  for(var i = 0; i < i3675.length; i += 1) {
    i3674.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+PhysicsSettings+CollisionMask', i3675[i + 0]) );
  }
  i3672.collisionMatrix = i3674
  return i3672
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+PhysicsSettings+CollisionMask"] = function (request, data, root) {
  var i3678 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+PhysicsSettings+CollisionMask' )
  var i3679 = data
  i3678.enabled = !!i3679[0]
  i3678.layerId = i3679[1]
  i3678.otherLayerId = i3679[2]
  return i3678
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+Physics2DSettings"] = function (request, data, root) {
  var i3680 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+Physics2DSettings' )
  var i3681 = data
  request.r(i3681[0], i3681[1], 0, i3680, 'material')
  i3680.gravity = new pc.Vec2( i3681[2], i3681[3] )
  i3680.positionIterations = i3681[4]
  i3680.velocityIterations = i3681[5]
  i3680.velocityThreshold = i3681[6]
  i3680.maxLinearCorrection = i3681[7]
  i3680.maxAngularCorrection = i3681[8]
  i3680.maxTranslationSpeed = i3681[9]
  i3680.maxRotationSpeed = i3681[10]
  i3680.timeToSleep = i3681[11]
  i3680.linearSleepTolerance = i3681[12]
  i3680.angularSleepTolerance = i3681[13]
  i3680.defaultContactOffset = i3681[14]
  i3680.autoSimulation = !!i3681[15]
  i3680.queriesHitTriggers = !!i3681[16]
  i3680.queriesStartInColliders = !!i3681[17]
  i3680.callbacksOnDisable = !!i3681[18]
  i3680.reuseCollisionCallbacks = !!i3681[19]
  i3680.autoSyncTransforms = !!i3681[20]
  var i3683 = i3681[21]
  var i3682 = []
  for(var i = 0; i < i3683.length; i += 1) {
    i3682.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+Physics2DSettings+CollisionMask', i3683[i + 0]) );
  }
  i3680.collisionMatrix = i3682
  return i3680
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+Physics2DSettings+CollisionMask"] = function (request, data, root) {
  var i3686 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+Physics2DSettings+CollisionMask' )
  var i3687 = data
  i3686.enabled = !!i3687[0]
  i3686.layerId = i3687[1]
  i3686.otherLayerId = i3687[2]
  return i3686
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.QualitySettings"] = function (request, data, root) {
  var i3688 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.QualitySettings' )
  var i3689 = data
  var i3691 = i3689[0]
  var i3690 = []
  for(var i = 0; i < i3691.length; i += 1) {
    i3690.push( request.d('Luna.Unity.DTO.UnityEngine.Assets.QualitySettings', i3691[i + 0]) );
  }
  i3688.qualityLevels = i3690
  var i3693 = i3689[1]
  var i3692 = []
  for(var i = 0; i < i3693.length; i += 1) {
    i3692.push( i3693[i + 0] );
  }
  i3688.names = i3692
  i3688.shadows = i3689[2]
  i3688.anisotropicFiltering = i3689[3]
  i3688.antiAliasing = i3689[4]
  i3688.lodBias = i3689[5]
  i3688.shadowCascades = i3689[6]
  i3688.shadowDistance = i3689[7]
  i3688.shadowmaskMode = i3689[8]
  i3688.shadowProjection = i3689[9]
  i3688.shadowResolution = i3689[10]
  i3688.softParticles = !!i3689[11]
  i3688.softVegetation = !!i3689[12]
  i3688.activeColorSpace = i3689[13]
  i3688.desiredColorSpace = i3689[14]
  i3688.masterTextureLimit = i3689[15]
  i3688.maxQueuedFrames = i3689[16]
  i3688.particleRaycastBudget = i3689[17]
  i3688.pixelLightCount = i3689[18]
  i3688.realtimeReflectionProbes = !!i3689[19]
  i3688.shadowCascade2Split = i3689[20]
  i3688.shadowCascade4Split = new pc.Vec3( i3689[21], i3689[22], i3689[23] )
  i3688.streamingMipmapsActive = !!i3689[24]
  i3688.vSyncCount = i3689[25]
  i3688.asyncUploadBufferSize = i3689[26]
  i3688.asyncUploadTimeSlice = i3689[27]
  i3688.billboardsFaceCameraPosition = !!i3689[28]
  i3688.shadowNearPlaneOffset = i3689[29]
  i3688.streamingMipmapsMemoryBudget = i3689[30]
  i3688.maximumLODLevel = i3689[31]
  i3688.streamingMipmapsAddAllCameras = !!i3689[32]
  i3688.streamingMipmapsMaxLevelReduction = i3689[33]
  i3688.streamingMipmapsRenderersPerFrame = i3689[34]
  i3688.resolutionScalingFixedDPIFactor = i3689[35]
  i3688.streamingMipmapsMaxFileIORequests = i3689[36]
  return i3688
}

Deserializers["Luna.Unity.DTO.UnityEngine.Assets.Mesh+BlendShapeFrame"] = function (request, data, root) {
  var i3698 = root || request.c( 'Luna.Unity.DTO.UnityEngine.Assets.Mesh+BlendShapeFrame' )
  var i3699 = data
  i3698.weight = i3699[0]
  i3698.vertices = i3699[1]
  i3698.normals = i3699[2]
  i3698.tangents = i3699[3]
  return i3698
}

Deserializers.fields = {"Luna.Unity.DTO.UnityEngine.Assets.Material":{"name":0,"shader":1,"renderQueue":3,"enableInstancing":4,"floatParameters":5,"colorParameters":6,"vectorParameters":7,"textureParameters":8,"materialFlags":9},"Luna.Unity.DTO.UnityEngine.Assets.Material+FloatParameter":{"name":0,"value":1},"Luna.Unity.DTO.UnityEngine.Assets.Material+ColorParameter":{"name":0,"value":1},"Luna.Unity.DTO.UnityEngine.Assets.Material+VectorParameter":{"name":0,"value":1},"Luna.Unity.DTO.UnityEngine.Assets.Material+TextureParameter":{"name":0,"value":1},"Luna.Unity.DTO.UnityEngine.Assets.Material+MaterialFlag":{"name":0,"enabled":1},"Luna.Unity.DTO.UnityEngine.Components.Transform":{"position":0,"scale":3,"rotation":6},"Luna.Unity.DTO.UnityEngine.Components.Camera":{"enabled":0,"aspect":1,"orthographic":2,"orthographicSize":3,"backgroundColor":4,"nearClipPlane":8,"farClipPlane":9,"fieldOfView":10,"depth":11,"clearFlags":12,"cullingMask":13,"rect":14,"targetTexture":15},"Luna.Unity.DTO.UnityEngine.Scene.GameObject":{"name":0,"tag":1,"enabled":2,"isStatic":3,"layer":4},"Luna.Unity.DTO.UnityEngine.Textures.Texture2D":{"name":0,"width":1,"height":2,"mipmapCount":3,"anisoLevel":4,"filterMode":5,"hdr":6,"format":7,"wrapMode":8,"alphaIsTransparency":9,"alphaSource":10},"Luna.Unity.DTO.UnityEngine.Assets.Mesh":{"name":0,"halfPrecision":1,"vertexCount":2,"aabb":3,"streams":4,"vertices":5,"subMeshes":6,"bindposes":7,"blendShapes":8},"Luna.Unity.DTO.UnityEngine.Assets.Mesh+SubMesh":{"triangles":0},"Luna.Unity.DTO.UnityEngine.Assets.Mesh+BlendShape":{"name":0,"frames":1},"Luna.Unity.DTO.UnityEngine.Components.RectTransform":{"pivot":0,"anchorMin":2,"anchorMax":4,"sizeDelta":6,"anchoredPosition3D":8,"rotation":11,"scale":15},"Luna.Unity.DTO.UnityEngine.Components.Canvas":{"enabled":0,"planeDistance":1,"referencePixelsPerUnit":2,"isFallbackOverlay":3,"renderMode":4,"renderOrder":5,"sortingLayerName":6,"sortingOrder":7,"scaleFactor":8,"worldCamera":9,"overrideSorting":11,"pixelPerfect":12,"targetDisplay":13,"overridePixelPerfect":14},"Luna.Unity.DTO.UnityEngine.Components.CanvasRenderer":{"cullTransparentMesh":0},"Luna.Unity.DTO.UnityEngine.Components.Animator":{"animatorController":0,"updateMode":2,"humanBones":3,"enabled":4},"Luna.Unity.DTO.UnityEngine.Components.SpriteRenderer":{"enabled":0,"sharedMaterial":1,"sharedMaterials":3,"receiveShadows":4,"shadowCastingMode":5,"sortingLayerID":6,"sortingOrder":7,"lightmapIndex":8,"lightmapSceneIndex":9,"lightmapScaleOffset":10,"lightProbeUsage":14,"reflectionProbeUsage":15,"color":16,"sprite":20,"flipX":22,"flipY":23,"drawMode":24,"size":25,"tileMode":27,"adaptiveModeThreshold":28,"maskInteraction":29,"spriteSortPoint":30},"Luna.Unity.DTO.UnityEngine.Components.CapsuleCollider":{"center":0,"radius":3,"height":4,"direction":5,"enabled":6,"isTrigger":7,"material":8},"Luna.Unity.DTO.UnityEngine.Components.MeshFilter":{"sharedMesh":0},"Luna.Unity.DTO.UnityEngine.Components.MeshRenderer":{"additionalVertexStreams":0,"enabled":2,"sharedMaterial":3,"sharedMaterials":5,"receiveShadows":6,"shadowCastingMode":7,"sortingLayerID":8,"sortingOrder":9,"lightmapIndex":10,"lightmapSceneIndex":11,"lightmapScaleOffset":12,"lightProbeUsage":16,"reflectionProbeUsage":17},"Luna.Unity.DTO.UnityEngine.Components.VideoPlayer":{"enabled":0,"source":1,"clip":2,"url":4,"playOnAwake":5,"isLooping":6,"renderMode":7,"targetMaterialRenderer":8,"targetMaterialProperty":10,"playbackSpeed":11},"Luna.Unity.DTO.UnityEngine.Textures.Cubemap":{"name":0,"atlasId":1,"mipmapCount":2,"hdr":3,"size":4,"anisoLevel":5,"filterMode":6,"wrapMode":7,"rects":8},"Luna.Unity.DTO.UnityEngine.Scene.Scene":{"name":0,"index":1,"startup":2},"Luna.Unity.DTO.UnityEngine.Assets.RenderSettings":{"ambientIntensity":0,"reflectionIntensity":1,"ambientMode":2,"ambientLight":3,"ambientSkyColor":7,"ambientGroundColor":11,"ambientEquatorColor":15,"fogColor":19,"fogEndDistance":23,"fogStartDistance":24,"fogDensity":25,"fog":26,"skybox":27,"fogMode":29,"lightmaps":30,"lightProbes":31,"lightmapsMode":32,"environmentLightingMode":33,"ambientProbe":34,"customReflection":35,"defaultReflection":37,"defaultReflectionMode":39,"defaultReflectionResolution":40,"sunLightObjectId":41,"pixelLightCount":42,"defaultReflectionHDR":43,"hasLightDataAsset":44,"hasManualGenerate":45},"Luna.Unity.DTO.UnityEngine.Assets.RenderSettings+Lightmap":{"lightmapColor":0,"lightmapDirection":2},"Luna.Unity.DTO.UnityEngine.Assets.RenderSettings+LightProbes":{"bakedProbes":0,"positions":1,"hullRays":2,"tetrahedra":3,"neighbours":4,"matrices":5},"Luna.Unity.DTO.UnityEngine.Assets.Shader":{"invalidShaderVariants":0,"name":1,"shaderDefinedKeywords":2,"passes":3,"defaultParameterValues":4,"unityFallbackShader":5,"readDepth":7,"IsCreatedByShaderGraph":8},"Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass":{"passType":0,"zTest":1,"zWrite":2,"culling":3,"blending":4,"alphaBlending":5,"colorWriteMask":6,"offsetUnits":7,"offsetFactor":8,"stencilRef":9,"stencilReadMask":10,"stencilWriteMask":11,"stencilOp":12,"stencilOpFront":13,"stencilOpBack":14,"tags":15,"passDefinedKeywords":16,"variants":17,"readDepth":18},"Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Value":{"val":0,"name":1},"Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Blending":{"src":0,"dst":1,"op":2},"Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+StencilOp":{"pass":0,"fail":1,"zFail":2,"comp":3},"Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Tag":{"name":0,"value":1},"Luna.Unity.DTO.UnityEngine.Assets.Shader+Pass+Variant":{"keywords":0,"vertexProgram":1,"fragmentProgram":2,"readDepth":3},"Luna.Unity.DTO.UnityEngine.Assets.Shader+DefaultParameterValue":{"name":0,"type":1,"value":2,"textureValue":6},"Luna.Unity.DTO.UnityEngine.Textures.Sprite":{"name":0,"texture":1,"aabb":3,"vertices":4,"triangles":5,"textureRect":6,"packedRect":10,"border":14,"transparency":18,"bounds":19,"pixelsPerUnit":20,"textureWidth":21,"textureHeight":22,"nativeSize":23,"pivot":25,"textureRectOffset":27},"Luna.Unity.DTO.UnityEngine.Assets.VideoClip":{"name":0,"width":1,"height":2,"frameRate":3,"frameCount":4},"Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationClip":{"name":0,"wrapMode":1,"isLooping":2,"length":3,"curves":4,"events":5,"halfPrecision":6},"Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationCurve":{"path":0,"componentType":1,"property":2,"keys":3,"objectReferenceKeys":4},"Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationCurve+ObjectReferenceKey":{"time":0,"value":1},"Luna.Unity.DTO.UnityEngine.Animation.Data.AnimationEvent":{"functionName":0,"floatParameter":1,"intParameter":2,"stringParameter":3,"objectReferenceParameter":4,"time":6},"Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorController":{"name":0,"states":1,"layers":2,"parameters":3,"animationClips":4},"Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorState":{"cycleOffset":0,"cycleOffsetParameter":1,"cycleOffsetParameterActive":2,"mirror":3,"mirrorParameter":4,"mirrorParameterActive":5,"motionId":6,"nameHash":7,"fullPathHash":8,"speed":9,"speedParameter":10,"speedParameterActive":11,"tag":12,"name":13,"writeDefaultValues":14,"transitions":15,"behaviours":16},"Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorStateTransition":{"fullPath":0,"canTransitionToSelf":1,"duration":2,"exitTime":3,"hasExitTime":4,"hasFixedDuration":5,"interruptionSource":6,"offset":7,"orderedInterruption":8,"destinationStateNameHash":9,"destinationStateMachineId":10,"isExit":11,"mute":12,"solo":13,"conditions":14},"Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorControllerLayer":{"blendingMode":0,"defaultWeight":1,"name":2,"stateMachine":3},"Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorStateMachine":{"id":0,"defaultStateNameHash":1,"entryTransitions":2,"anyStateTransitions":3},"Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorTransition":{"destinationStateNameHash":0,"destinationStateMachineId":1,"isExit":2,"mute":3,"solo":4,"conditions":5},"Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorCondition":{"mode":0,"parameter":1,"threshold":2},"Luna.Unity.DTO.UnityEngine.Animation.Mecanim.AnimatorControllerParameter":{"defaultBool":0,"defaultFloat":1,"defaultInt":2,"name":3,"nameHash":4,"type":5},"Luna.Unity.DTO.UnityEngine.Assets.Resources":{"files":0,"componentToPrefabIds":1},"Luna.Unity.DTO.UnityEngine.Assets.Resources+File":{"path":0,"unityObject":1},"Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings":{"scriptsExecutionOrder":0,"sortingLayers":1,"cullingLayers":2,"timeSettings":3,"physicsSettings":4,"physics2DSettings":5,"qualitySettings":6,"removeShadows":7,"autoInstantiatePrefabs":8,"enableAutoInstancing":9,"lightmapEncodingQuality":10,"desiredColorSpace":11},"Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+ScriptsExecutionOrder":{"name":0,"value":1},"Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+SortingLayer":{"id":0,"name":1,"value":2},"Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+CullingLayer":{"id":0,"name":1},"Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+TimeSettings":{"fixedDeltaTime":0,"maximumDeltaTime":1,"timeScale":2,"maximumParticleTimestep":3},"Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+PhysicsSettings":{"gravity":0,"defaultSolverIterations":3,"bounceThreshold":4,"autoSyncTransforms":5,"autoSimulation":6,"collisionMatrix":7},"Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+PhysicsSettings+CollisionMask":{"enabled":0,"layerId":1,"otherLayerId":2},"Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+Physics2DSettings":{"material":0,"gravity":2,"positionIterations":4,"velocityIterations":5,"velocityThreshold":6,"maxLinearCorrection":7,"maxAngularCorrection":8,"maxTranslationSpeed":9,"maxRotationSpeed":10,"timeToSleep":11,"linearSleepTolerance":12,"angularSleepTolerance":13,"defaultContactOffset":14,"autoSimulation":15,"queriesHitTriggers":16,"queriesStartInColliders":17,"callbacksOnDisable":18,"reuseCollisionCallbacks":19,"autoSyncTransforms":20,"collisionMatrix":21},"Luna.Unity.DTO.UnityEngine.Assets.ProjectSettings+Physics2DSettings+CollisionMask":{"enabled":0,"layerId":1,"otherLayerId":2},"Luna.Unity.DTO.UnityEngine.Assets.QualitySettings":{"qualityLevels":0,"names":1,"shadows":2,"anisotropicFiltering":3,"antiAliasing":4,"lodBias":5,"shadowCascades":6,"shadowDistance":7,"shadowmaskMode":8,"shadowProjection":9,"shadowResolution":10,"softParticles":11,"softVegetation":12,"activeColorSpace":13,"desiredColorSpace":14,"masterTextureLimit":15,"maxQueuedFrames":16,"particleRaycastBudget":17,"pixelLightCount":18,"realtimeReflectionProbes":19,"shadowCascade2Split":20,"shadowCascade4Split":21,"streamingMipmapsActive":24,"vSyncCount":25,"asyncUploadBufferSize":26,"asyncUploadTimeSlice":27,"billboardsFaceCameraPosition":28,"shadowNearPlaneOffset":29,"streamingMipmapsMemoryBudget":30,"maximumLODLevel":31,"streamingMipmapsAddAllCameras":32,"streamingMipmapsMaxLevelReduction":33,"streamingMipmapsRenderersPerFrame":34,"resolutionScalingFixedDPIFactor":35,"streamingMipmapsMaxFileIORequests":36},"Luna.Unity.DTO.UnityEngine.Assets.Mesh+BlendShapeFrame":{"weight":0,"vertices":1,"normals":2,"tangents":3}}

Deserializers.requiredComponents = {"35":[36],"37":[36],"38":[36],"39":[36],"40":[36],"41":[36],"42":[43],"44":[2],"45":[46],"47":[46],"48":[46],"49":[46],"50":[46],"51":[46],"52":[46],"53":[54],"55":[54],"56":[54],"57":[54],"58":[54],"59":[54],"60":[54],"61":[54],"62":[54],"63":[54],"64":[54],"65":[54],"66":[54],"67":[2],"68":[31],"69":[70],"71":[70],"5":[4],"72":[4],"73":[11,4],"74":[31],"75":[11,4],"76":[4],"77":[31,4],"78":[4,11],"79":[4],"80":[4],"81":[4],"8":[5],"13":[11,4],"19":[4],"7":[5],"82":[4],"83":[4],"84":[4],"85":[4],"86":[4],"87":[4],"88":[4],"89":[4],"90":[4],"12":[11,4],"91":[4],"92":[4],"93":[4],"94":[4],"95":[11,4],"96":[4],"97":[27],"98":[27],"28":[27],"99":[27],"100":[2],"101":[2]}

Deserializers.types = ["UnityEngine.Shader","UnityEngine.Transform","UnityEngine.Camera","UnityEngine.AudioListener","UnityEngine.RectTransform","UnityEngine.Canvas","UnityEngine.EventSystems.UIBehaviour","UnityEngine.UI.CanvasScaler","UnityEngine.UI.GraphicRaycaster","UnityEngine.MonoBehaviour","UnityEngine.EventSystems.EventTrigger","UnityEngine.CanvasRenderer","UnityEngine.UI.RawImage","UnityEngine.UI.Image","UnityEngine.Sprite","UnityEngine.Animator","UnityEditor.Animations.AnimatorController","UnityEngine.UI.Button","GameManager","UnityEngine.UI.AspectRatioFitter","UnityEngine.SpriteRenderer","UnityEngine.Material","UnityEngine.CapsuleCollider","UnityEngine.GameObject","UnityEngine.Video.VideoPlayer","UnityEngine.Video.VideoClip","TouchInputManager","UnityEngine.EventSystems.EventSystem","UnityEngine.EventSystems.StandaloneInputModule","UnityEngine.MeshFilter","UnityEngine.Mesh","UnityEngine.MeshRenderer","UnityEngine.Cubemap","DG.Tweening.Core.DOTweenSettings","UnityEngine.Texture2D","UnityEngine.AudioLowPassFilter","UnityEngine.AudioBehaviour","UnityEngine.AudioHighPassFilter","UnityEngine.AudioReverbFilter","UnityEngine.AudioDistortionFilter","UnityEngine.AudioEchoFilter","UnityEngine.AudioChorusFilter","UnityEngine.Cloth","UnityEngine.SkinnedMeshRenderer","UnityEngine.FlareLayer","UnityEngine.ConstantForce","UnityEngine.Rigidbody","UnityEngine.Joint","UnityEngine.HingeJoint","UnityEngine.SpringJoint","UnityEngine.FixedJoint","UnityEngine.CharacterJoint","UnityEngine.ConfigurableJoint","UnityEngine.CompositeCollider2D","UnityEngine.Rigidbody2D","UnityEngine.Joint2D","UnityEngine.AnchoredJoint2D","UnityEngine.SpringJoint2D","UnityEngine.DistanceJoint2D","UnityEngine.FrictionJoint2D","UnityEngine.HingeJoint2D","UnityEngine.RelativeJoint2D","UnityEngine.SliderJoint2D","UnityEngine.TargetJoint2D","UnityEngine.FixedJoint2D","UnityEngine.WheelJoint2D","UnityEngine.ConstantForce2D","UnityEngine.StreamingController","UnityEngine.TextMesh","UnityEngine.Tilemaps.TilemapRenderer","UnityEngine.Tilemaps.Tilemap","UnityEngine.Tilemaps.TilemapCollider2D","TMPro.TMP_Dropdown","TMPro.TMP_SelectionCaret","TMPro.TMP_SubMesh","TMPro.TMP_SubMeshUI","TMPro.TMP_Text","TMPro.TextMeshPro","TMPro.TextMeshProUGUI","TMPro.TextContainer","UnityEngine.UI.Dropdown","UnityEngine.UI.Graphic","UnityEngine.UI.ContentSizeFitter","UnityEngine.UI.GridLayoutGroup","UnityEngine.UI.HorizontalLayoutGroup","UnityEngine.UI.HorizontalOrVerticalLayoutGroup","UnityEngine.UI.LayoutElement","UnityEngine.UI.LayoutGroup","UnityEngine.UI.VerticalLayoutGroup","UnityEngine.UI.Mask","UnityEngine.UI.MaskableGraphic","UnityEngine.UI.RectMask2D","UnityEngine.UI.ScrollRect","UnityEngine.UI.Scrollbar","UnityEngine.UI.Slider","UnityEngine.UI.Text","UnityEngine.UI.Toggle","UnityEngine.EventSystems.BaseInputModule","UnityEngine.EventSystems.PointerInputModule","UnityEngine.EventSystems.TouchInputModule","UnityEngine.EventSystems.Physics2DRaycaster","UnityEngine.EventSystems.PhysicsRaycaster"]

Deserializers.unityVersion = "2020.3.24f1";

Deserializers.productName = "MPS_Luna";

Deserializers.lunaInitializationTime = "01/07/2022 07:42:01";

Deserializers.lunaVersion = "2.10.0";

Deserializers.lunaSHA = "143b266f4fdc2e6b3f807408df5fd3a5aa7a8444";

Deserializers.packagesInfo = "com.unity.2d.sprite: 1.0.0,com.unity.2d.tilemap: 1.0.0,com.unity.collab-proxy: 1.15.4,com.unity.ide.visualstudio: 2.0.12,com.unity.ide.vscode: 1.2.4,com.unity.test-framework: 1.1.29,com.unity.textmeshpro: 3.0.6,com.unity.timeline: 1.4.8,com.unity.ugui: 1.0.0,uk.lunalabs.luna: file:C:/Users/ADMIN/Downloads/2.10.0/scripts,com.unity.modules.ai: 1.0.0,com.unity.modules.androidjni: 1.0.0,com.unity.modules.animation: 1.0.0,com.unity.modules.assetbundle: 1.0.0,com.unity.modules.audio: 1.0.0,com.unity.modules.cloth: 1.0.0,com.unity.modules.director: 1.0.0,com.unity.modules.imageconversion: 1.0.0,com.unity.modules.imgui: 1.0.0,com.unity.modules.jsonserialize: 1.0.0,com.unity.modules.particlesystem: 1.0.0,com.unity.modules.physics: 1.0.0,com.unity.modules.physics2d: 1.0.0,com.unity.modules.screencapture: 1.0.0,com.unity.modules.terrain: 1.0.0,com.unity.modules.terrainphysics: 1.0.0,com.unity.modules.tilemap: 1.0.0,com.unity.modules.ui: 1.0.0,com.unity.modules.uielements: 1.0.0,com.unity.modules.umbra: 1.0.0,com.unity.modules.unityanalytics: 1.0.0,com.unity.modules.unitywebrequest: 1.0.0,com.unity.modules.unitywebrequestassetbundle: 1.0.0,com.unity.modules.unitywebrequestaudio: 1.0.0,com.unity.modules.unitywebrequesttexture: 1.0.0,com.unity.modules.unitywebrequestwww: 1.0.0,com.unity.modules.vehicles: 1.0.0,com.unity.modules.video: 1.0.0,com.unity.modules.vr: 1.0.0,com.unity.modules.wind: 1.0.0,com.unity.modules.xr: 1.0.0";

Deserializers.companyName = "DefaultCompany";

Deserializers.buildPlatform = "StandaloneWindows64";

Deserializers.applicationIdentifier = "com.Company.ProductName";

Deserializers.disableAntiAliasing = true;

Deserializers.typeNameToIdMap = function(){ var i = 0; return Deserializers.types.reduce( function( res, item ) { res[ item ] = i++; return res; }, {} ) }()

