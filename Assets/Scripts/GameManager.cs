﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using DG.Tweening;
using System;

public class GameManager : MonoBehaviour
{
    public GameObject UIElems;
    public GameObject bars;
    public GameObject endUI;
    public GameObject CTA;

    private TouchInputManager TouchInputManager;

    public VideoPlayer video;
    //public float videoPlayTime; // in seconds
    public bool isContinue = false;
    private bool isWin = true;

    public bool alwaysFail = false;
    public bool nextFail = false;
    public bool isShootFail = false;

    private VideoClip normClip;
    private VideoClip failClip;
    private VideoClip shootFailClip;

    public VideoClip normClipLandscape;
    public VideoClip failClipLandscape;
    public VideoClip shootFailClipLandscape;

    public VideoClip normClipPortrait;
    public VideoClip failClipPortrait;
    public VideoClip shootFailClipPortrait;

    public GameObject HorizontalCollider;
    public GameObject VerticalCollider;

    public Image timeBar;
    public float timeLimit = 0.1f;

    private void Awake()
    {
        if (Screen.width > Screen.height)
        {
            Debug.Log("Screen is landscape");
            normClip = normClipLandscape;
            failClip = failClipLandscape;
            shootFailClip = shootFailClipLandscape;
            HorizontalCollider.SetActive(true);

            video.gameObject.transform.localScale = new Vector3((float)Convert.ToDouble(18.08711), (float)Convert.ToDouble(10.19995), 0);
        }
        else
        {
            Debug.Log("Screen is portrait");
            normClip = normClipPortrait;
            failClip = failClipPortrait;
            shootFailClip = shootFailClipPortrait;
            VerticalCollider.SetActive(true);

            video.gameObject.transform.localScale = new Vector3((float)Convert.ToDouble(5.687767), (float)Convert.ToDouble(10.19995), 0);
        }

        video.loopPointReached += VideoEnded;
        TouchInputManager = GetComponent<TouchInputManager>();
    }

    Tween timeBarTween;

    // Start is called before the first frame update
    void Start()
    {
        timeBar.transform.parent.gameObject.SetActive(false);        

        video.clip = normClip;
        //videoPlayTime = video.frameCount / video.frameRate;
      /*  Debug.Log("video at 7 sec = " + video.frameRate * 7);*/
        
        video.Play();
    }

    // Update is called once per frame
    void Update()
    {        
        //Debug.Log(video.frame);
        if (!isContinue && video.isPlaying)
        {
            if ((float)Convert.ToDouble(video.frame) >= 7.2f * (float)Convert.ToDouble(video.frameRate))
            {
                video.Pause();
                UIElems.SetActive(true);

                timeBar.transform.parent.gameObject.SetActive(true);
                timeBarTween = timeBar.DOFillAmount(0, timeLimit).OnComplete(() =>
                {
                    timeBar.transform.parent.gameObject.SetActive(false);
                    UIElems.SetActive(false);
                    video.clip = failClip;
                    isWin = false;
                    video.Play();
                });
            }
        }
        if (isContinue && !nextFail && isShootFail)
        {
            if ((float)Convert.ToDouble(video.frame) > 3.9f * (float)Convert.ToDouble(video.frameRate))
            {
                nextFail = true;
                /*  Debug.Log("video fail" + video.frame);*/
                //video.Stop();
                video.clip = failClip;
                video.Play();
            }
        }
    }

    public void Shoot() {
        timeBarTween.Kill();
        timeBar.transform.parent.gameObject.SetActive(false);

        if (video.isPaused) {
            UIElems.SetActive(false);

            //Debug.Log("shoot collider: " + CheckColliderWithName("Collider"));

            if (alwaysFail)
            {
                video.clip = shootFailClip;
                //video.Stop();
                isWin = false;
                isShootFail = true;
            } else
            {
                if (!CheckColliderWithName("Collider"))
                {
                    video.clip = shootFailClip;
                    isWin = false;
                   // video.Stop();
                    isShootFail = true;
                }
            }
            isContinue = true;
            //video.Stop();
            video.Play();           
        }
    }
    private void VideoEnded(VideoPlayer source)
    {
        CTA.SetActive(true);
        bars.SetActive(false);
        if (!isWin)
            endUI.SetActive(true);
        Luna.Unity.LifeCycle.GameEnded();
    }

    public bool CheckColliderWithName(string name)
    {
        if (Physics.Raycast(TouchInputManager.Scope.position, Camera.main.transform.forward, out RaycastHit hit)) {
            Debug.DrawRay(TouchInputManager.Scope.position, Camera.main.transform.forward * hit.distance, Color.yellow);
            //Debug.Log("hit name = " + hit.collider.gameObject.name);
            if (hit.collider.gameObject.name == name) return true;
        }

        return false;
    }

    public void CTAClick()
    {
        Luna.Unity.Playable.InstallFullGame();
    }
}
